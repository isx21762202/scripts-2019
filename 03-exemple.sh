#! /bin/bash


# Validar argumento
if [ $# -ne 1]; then
	echo "Error: n#args incorrectes"
	echo "Usage: $0 edat"
	exit 1
fi

# chicha

edat=$1
if [ $edat -ge 18 ]; then
	echo "edat $edat es major d'edat"
fi
exit 0
